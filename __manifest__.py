{
    'name': 'Website Animejs',
    'author': "Jorels SAS",
    'category': 'Website',
    'summary': 'Use Animejs library',
    'version': '12.0',
    'description': """
        Animejs library support
    """,
    'depends': ['website'],
    'data': [
        'views/website_animejs.xml',
    ],
    'installable': True,
    'auto_install': True,
}
